import { createRouter, createWebHashHistory } from 'vue-router'
import Home from '../views/Home.vue'

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/gene/:id',
    name: 'gene',
    props: true,
    component: () => import('../views/Gene.vue')
  },
  {
    path: '/download',
    name: 'download',
    component: () => import('../views/Download.vue')
  },
  {
    path: '/overview',
    name: 'overview',
    component: () => import('../views/Overview.vue')
  },
  {
    path: '/abbreviations',
    name: 'abbreviations',
    component: () => import('../views/Abbreviations.vue')
  }
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

export default router
