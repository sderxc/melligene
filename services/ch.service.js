"use strict";
const { ClickHouse } = require('clickhouse');
module.exports = {
	clickhouse: null,
	name: "ch",

	/**
	 * Settings
	 */
	settings: {
	},

	/**
	 * Action Hooks
	 */
	hooks: {
	},

	/**
	 * Actions
	 */
	actions: {
		geneInfo: {
			params: {
				geneId: { type: "string", optional: true },
			},
			async handler(ctx) {
				const gene = await this.clickhouse.query(`SELECT * FROM genes.genes_storage WHERE \`Ensembl\` = '${ctx.params.geneId}'`).toPromise();
				return gene[0];
			}
		},
		 meta: {
			async handler(ctx) {
				const tumorTypes = await this.clickhouse.query('SELECT DISTINCT TumorType FROM genes.genes_expression').toPromise();
				const geneOrigins = await this.clickhouse.query('SELECT DISTINCT Geneorigin FROM genes.genes_storage').toPromise();
				return {
					tumorTypes: tumorTypes.map(a => a.TumorType),
					geneOrigins: geneOrigins.map(a => a.Geneorigin),
				};
			}
		},
		search: {
			cache: true,
			params: {
				offset: {type: "string", optional:true},
				fpkm: { type: "string", optional: true },
				fpkmLess: { type: "string", optional: true },
				originIn: {type: "array", optional: true},
				originOut: {type: "array", optional: true},
				tumorTypeIn: {type: "array", optional: true},
				tumorTypeOut: {type: "array", optional: true},
				search: { type: "string", optional: true },
			},
			async handler(ctx) {
				const limit = 20;
				let where = '';
				let subwhere = '';
				if (ctx.params.fpkm) {
					where+= `WHERE \n \`FPKM\` >= ${ctx.params.fpkm} \n `;
				}
				if (ctx.params.fpkmLess) {
					if (where) {
						where += ' AND ';
					} else {
						where += ' WHERE ';
					}
					where+= `\`FPKM\` <= ${ctx.params.fpkmLess} \n `;
				}

				if (ctx.params.tumorTypeIn) {
					if (where) {
						where += ' AND ';
					} else {
						where += ' WHERE ';
					}
					where+= `\`TumorType\` IN ('${ctx.params.tumorTypeIn.join ('\', \'')}') `;
				}

				if (ctx.params.tumorTypeOut) {
					if (where) {
						where += ' AND ';
					} else {
						where += ' WHERE ';
					}
					where+= `\`TumorType\` NOT IN ('${ctx.params.tumorTypeOut.join ('\', \'')}') `;
				}

				if (ctx.params.originIn) {
					subwhere += `Geneorigin IN ('${ctx.params.originIn.join ('\', \'')}') `;
				}

				if (ctx.params.originOut) {
					if (subwhere) {
						subwhere += ' AND ';
					}
					subwhere += `Geneorigin NOT IN ('${ctx.params.originOut.join ('\', \'')}') `;
				}

				if (ctx.params.search) {
					if (subwhere) {
						subwhere += ' AND ';
					}
					subwhere += `( Uniprot LIKE '%${ctx.params.search}%' `;
					subwhere += `OR Genename LIKE '%${ctx.params.search}%' `;
					subwhere += `OR Ensembl LIKE '%${ctx.params.search}%') `;
				}

				if (subwhere) {
					if (!where) {
						where += 'WHERE ';
					} else {
						where += 'AND ';
					}
					where+= `\`genes_expression\`.\`Genename\` IN (SELECT DISTINCT \`Ensembl\` FROM \`genes_storage\` WHERE ${subwhere} ORDER BY \`Ensembl\` )  `;
				}

				const json = await this.clickhouse.query('SELECT DISTINCT\n' +
					'    `genes_expression`.`Genename`,\n' +
					'    `genes_expression`.`TumorType`,\n' +
					'    COUNT(`genes_expression`.`FPKM`) as total,\n' +
					'    MAX(`genes_expression`.`FPKM`) as max,\n' +
					'    MIN(`genes_expression`.`FPKM`) as min,\n' +
					'    AVG(`genes_expression`.`FPKM`) as avg\n' +
					'FROM `genes_expression`\n' + where +
					'GROUP BY `Genename`, `TumorType` \n' +
					'ORDER BY `genes_expression`.`Genename` \n' +
					'LIMIT ' + (ctx.params.offset*limit) + ', ' + limit

				).toPromise();

				return json;
			}
		},
	},

	/**
	 * Methods
	 */
	methods: {
	},
	async created() {
		//CREATE DATABASE IF NOT EXISTS `genes`;
		// 	CREATE TABLE IF NOT EXISTS `genes_storage`
		// (
		//   Uniprot          String,
		//   Genename         String,
		//   Geneage          Float32,
		//   Geneorigin       String,
		//   Ensembl          String,
		//   Ensembl_v        String,
		//   Description      String,
		//   Chromosome_name  String,
		//   Gene_type        String
		// ) ENGINE = MergeTree()
		// ORDER BY (Geneorigin)

		// cat data/genes.csv | curl 'http://localhost:8123/?query=INSERT%20INTO%20genes.genes_storage%20FORMAT%20CSVWithNames' --data-binary @-
		// 	CREATE TABLE IF NOT EXISTS `genes_expression`
		// 	(
		// 		Genename         String,
		// 		FPKM             Float64,
		// 		TumorType        String
		// ) ENGINE = MergeTree()
		// 	ORDER BY (TumorType)

		this.clickhouse = new ClickHouse({
			url: 'melligene_clickhouse',
			port: 8123,
			debug: false,
			basicAuth: null,
			isUseGzip: false,
			format: "json", // "json" || "csv" || "tsv"
			raw: false,
			config: {
				session_timeout: 60,
				output_format_json_quote_64bit_integers: 0,
				enable_http_compression: 0,
				database: 'genes',
			},
		});

	},
};
