#!/bin/sh
npm install

if [ "$APP_ENV" == "local" ]
then
  npm run dev & npm run serve
else
  npm run build
  npm run start
fi

